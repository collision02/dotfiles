STOW=stow --verbose=1 -t "$(HOME)"
MODULES=bin common emacs erlang git openbox sbcl systemd x11 zsh

.PHONY: all $(MODULES)
all: $(MODULES)

$(MODULES):
	$(STOW) $@
