#!/usr/bin/env zsh

export HISTFILE="${HOME}/.zhistory"
export HISTSIZE=1500
export SAVEHIST=10000

autoload -Uz compinit promptinit colors
compinit
promptinit
colors

case $TERM in
  xterm*)
    precmd () {print -Pn "\e]0;%~\a"}
    ;;
esac

zstyle :compinstall filename "${HOME}/.zshrc"
zstyle ':completion:*' menu select # select completions with arrow keys
zstyle ':completion:*' group-name '' # group results by category
zstyle ':completion:::::' completer _expand _complete _ignored _approximate # enable approximate matches for completion


setopt appendhistory autocd notify COMPLETE_ALIASES
setopt hist_ignore_all_dups # remove older duplicate entries from history
setopt hist_reduce_blanks # remove superfluous blanks from history items
setopt inc_append_history # save history entries as soon as they are entered
setopt share_history # share history between different instances of the shell
setopt auto_cd # cd by typing directory name if it's not a command
setopt auto_list # automatically list choices on ambiguous completion
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match

bindkey -e
bindkey '^[[3~' delete-char
bindkey '^[3;5~' delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

source "${HOME}/.zsh_prompt"
