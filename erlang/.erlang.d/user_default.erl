-module(user_default).
-export([
    help/0,
    cmd/1,
    ld/1,
    f5/1,
    r3/1,
    r3/2,
    r3lc/0
]).

-define(HELP, [
        {cmd,   "(Command)",                                "runs Command in OS shell"},
        {ld,    "(Module = atom())",                        "adds Module to Erlang path and loads it"},
        {f5,    "(Module = atom())",                        "reloads Module from file"},
        {r3,    "(Action = string())",                      "rebar3 Action"},
        {r3,    "(Profile = string(), Action = string())",  "rebar3 as Profie Action"},
        {r3lc,  "()",                                       "rebar3 as local compile"}
]).

help() ->
    io:format("*** custom commands ***~n~n", []),
    lists:foreach(fun({Name, Arity, Help}) ->
        io:format("~ts~ts -- ~ts~n", [erlang:atom_to_list(Name), Arity, Help])
    end, ?HELP),

    io:format("~n~n", []),

    shell_default:help(),

    io:format("~n~n", []),
    ok.

path() ->
    {ok, [[Home]]} = init:get_argument(home),
    filename:join([Home, ".erlang.d", "deps"]).

cmd(Cmd) -> io:format("$> ~ts~n", [os:cmd(Cmd)]).

ld(Module) ->
    ld(erlang:atom_to_list(Module), Module).
ld(Dir, Module) ->
    case code:is_loaded(Module) of
        false ->
            Base = filename:join([path(), Dir]),
            Files = filelib:wildcard(filename:join([Base, "**/", erlang:atom_to_list(Module) ++ ".beam"])),
            io:format("Files:\t~p~n", [Files]),
            case length(Files) of
                0 ->
                    code:add_path(Base);
                _ ->
                    [File | _] = Files,
                    code:add_path(filename:dirname(File))
            end,
            code:load_file(Module);
        _ ->
            code:purge(Module),
            ld(Dir, Module)
    end.

f5(Module) ->
    case code:is_loaded(Module) of
        {file, AbsPath} ->
            code:purge(Module),
            compile:file(filelib:find_source(AbsPath)) =:= error andalso throw({error, compile, Module}),
            code:load_file(Module),
            {ok, Module};
        _ ->
            case catch code:load_file(Module) of
                {module, Module} -> {ok, Module};
                _ -> {not_loaded, Module}
            end
    end.

r3(Action) ->
    cmd("rebar3 " ++ Action).

r3(Profile, Action) ->
    cmd("rebar3 as " ++ Profile ++ " " ++ Action).

r3lc() ->
    r3("local", "compile").
