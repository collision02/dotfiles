(global-set-key (kbd "<C-S-down>") 'co2/move-line-down)
(global-set-key (kbd "<C-S-up>") 'co2/move-line-up)
(global-set-key (kbd "C-c d") 'co2/duplicate-line)
(global-set-key (kbd "C-c k") 'delete-frame)

(provide 'keybindings)
