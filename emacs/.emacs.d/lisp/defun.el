(defun co2/move-line-down ()
    (interactive)
    (let ((col (current-column)))
      (save-excursion
        (forward-line)
        (transpose-lines 1))
      (forward-line)
      (move-to-column col)))

(defun co2/move-line-up ()
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines -1))
    (forward-line -1)
    (move-to-column col)))

(defun co2/duplicate-line ()
  (interactive)
  (let ((content (buffer-substring-no-properties (line-beginning-position)
                                                 (line-end-position))))
  (save-excursion
    (newline)
    (insert content))))

(defun co2/reload-config ()
  (interactive)
  (with-temp-buffer
    (insert-file-contents (expand-file-name "init.el" user-config-directory))
    (eval-buffer))
  (message "init.el reloaded!"))

(provide 'defun)
