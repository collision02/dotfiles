(use-package auto-package-update
  :ensure t
  :config (setq auto-package-update-delete-old-versions t
                auto-package-update-interval 4)
  :hook '(after-init . auto-package-update-maybe))

(use-package crux :ensure t)

(use-package helm
  :diminish helm-mode
  :ensure t
  :bind (("C-x C-f" . helm-find-files)
         ("M-x" . helm-M-x)
         ("C-x b" . helm-mini)
         ("C-x C-b" . helm-mini)
         ("M-y" . helm-show-kill-ring)
         :map helm-map
         ("<tab>" . helm-execute-persistent-action) ; Rebind TAB to expand
         ("C-i" . helm-execute-persistent-action) ; Make TAB work in CLI
         ("C-z" . helm-select-action)) ; List actions using C-z

  :config
  (setq-default helm-split-window-in-side-p t)
  (setq helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t
        helm-split-window-default-side 'other)
  (helm-mode 1))
n
(use-package company
  :ensure t
  :hook '(after-init . global-company-mode))

(use-package company-quickhelp
  :after company
  :ensure t
  :hook '(company-mode . company-quickhelp-mode))

(use-package which-key
  :ensure t
  :hook '(after-init . which-key-mode)
  :config (setq which-key-popup-type 'side-window
                which-key-side-window-location 'right
                which-key-side-window-max-width 0.4
                which-key-side-window-max-height 1)

  :bind ("C-c h" . which-key-show-top-level))

(use-package magit
  :ensure t
  :defer t
  :bind (("C-c m" . magit-status)))

(use-package hungry-delete
  :ensure t
  :config (global-hungry-delete-mode t))

(use-package org
  :mode (("\\.org\\'" . org-mode))
  :ensure org-plus-contrib
  :config (setq org-directory "~/org/"
                org-agenda-files '("~/org/todo.org")
                org-default-notes-file (concat org-directory "notes.org")
                org-log-done t
                org-pretty-entities t
                org-todo-keywords '("TODO(t)" "STARTED(s)" "WAITING(w)" "SOMEDAY(.)" "MAYBE(m)"
                                    "|" "DONE(x!)" "CANCELLED(c)")
                org-modules '(org-habit org-info org-tempo org-protocol)
                org-capture-templates '(("t" "Task" entry (file "~/org/todo.org")
                                         "** TODO %?\n")
                                        ("p" "Project" entry (file "~/org/todo.org")
                                         "* %?\n")
                                        ("b" "Bookmark" item (file+headline "~/org/notes.org" "Bookmarks")
                                         "%? [[%:link][%:description]] %U\n")
                                        ("n" "Note" entry (file+headline "~/org/notes.org" "Notes")
                                         "** %^{Title}\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n:PROPERTIES:\n:SOURCE: %u, $c\n:CREATED: %U\n:END:\n"))
                org-capture-default-template "b"
                org-refile-targets '((org-agenda-files :maxlevel . 3))
                org-refile-allow-creating-parent-nodes 'confirm)
  :bind (("C-c l" . 'org-store-link)
         ("C-c c" . 'org-capture)
         ("C-c a" . 'org-agenda)))

(use-package org-bullets
  :hook '(org-mode . (lambda () (org-bullets-mode 1))))

(use-package flycheck
  :ensure t
  :config (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)
                        flycheck-erlang-rebar3-executable "~/bin/rebar3")
  :hook '(prog-mode . flycheck-mode))

(use-package dired-sidebar
  :ensure t
  :init (dired-sidebar-toggle-sidebar)
  :config (setq dired-sidebar-should-follow-file t))

(provide 'packages-common)
