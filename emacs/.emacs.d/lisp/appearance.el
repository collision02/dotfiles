(setq inhibit-startup-screen t
      frame-title-format '(buffer-file-name "%f" ("%b")))
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(when (>= emacs-major-version 26)
  ;; (pixel-scroll-mode)
  (global-display-line-numbers-mode t))

;;; Globally enable syntax highlighting
(setq font-lock-maximum-decoration t
      show-paren-delay 0
      show-paren-when-point-inside-paren t
      show-trailing-whitespace t)

(setq-default cursor-type 'bar)

(global-font-lock-mode t)
(global-hl-line-mode 1)
(global-prettify-symbols-mode t)
(show-paren-mode 1)
(blink-cursor-mode 1)

(add-to-list 'custom-theme-load-path
             (expand-file-name "themes/" user-config-directory))

(defvar after-load-theme-hook nil
  "Hook run after a color theme is loaded using `load-theme'.")

(defadvice load-theme (after run-after-load-theme-hook activate)
  "Run `after-load-theme-hook'."
  (run-hooks 'after-load-theme-hook))

(defun co2/set-font ()
  "Configures the fonts"
  (when (window-system)
    (set-face-attribute 'default nil :family "Hack" :height 110)))

(add-hook 'after-load-theme-hook #'co2/set-font)

(use-package gruvbox-theme
  :ensure t
  :init (progn
          (load-theme 'gruvbox t)
          (co2/set-font)))

(provide 'appearance)
