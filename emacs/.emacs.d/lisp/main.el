;;; Disable autosave features.
(setq make-backup-files nil
      auto-save-default nil
      auto-save-list-file-prefix nil
      create-lockfiles nil
      ;;; Place backup files in specific directory.
      backup-directory-alist
      `(("." . ,(expand-file-name "backups" user-emacs-directory))))

(defalias 'yes-or-no-p 'y-or-n-p)

(setq-default tab-width 4)
(defvaralias 'standard-indent 'tab-width)
(setq-default indent-tabs-mode nil)

(setq require-final-newline t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq completion-ignore-case t
      read-file-name-completion-ignore-case t)

(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8
      coding-system-for-write 'utf-8)

(provide 'main)
