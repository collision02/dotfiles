(use-package erlang
  :ensure t
  :mode ("\\.[eh]rl\\'" . erlang-mode))

(use-package js2-mode
  :ensure t
  :mode ("\\.js\\'" . js2-mode))

(use-package json-mode
  :ensure t
  :mode ("\\.json\\'" . json-mode)
  :config (setq-default js-indent-level 2))

(use-package json-reformat
  :ensure t
  :after json-mode
  :bind ("C-c r" . json-reformat-region))

(use-package lua-mode
  :mode "\\.lua\\'")
  ;; :hook '(lua-mode-hook . (lambda () (unless (fboundp 'lua-calculate-indentation-right-shift-next)
  ;;                                 (load-file (locate-file "lua-mode-fix.el" load-path))))))

(use-package web-mode
  :ensure t
  :mode "\\.css\\'"
  :mode "\\.html?\\'")

(use-package yaml-mode
  :ensure t
  :mode "\\.yml\\'"
  :mode "\\.yaml\\'")

(use-package racket-mode
  :mode "\\.rkt\\'")

(use-package rust-mode
  :ensure t
  :mode "\\.rs\\'")

;; (use-package sly
;;   :config (setq inferior-lisp-program
;;                 (substring (shell-command-to-string "command -v sbcl") 0 -1)))

(provide 'packages-languages)
