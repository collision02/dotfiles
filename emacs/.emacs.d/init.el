(setq load-prefer-newer t)
(defvar user-config-directory nil)

(when (eq (symbol-value 'user-config-directory) nil)
  (progn
    (add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))
    (setq user-config-directory user-emacs-directory
          user-emacs-directory "~/.cache/emacs/")))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(when (require 'package nil t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (package-initialize)
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))
  (require 'use-package))

(require 'main)
(require 'appearance)
(require 'defun)
(require 'keybindings)
(require 'packages-common)
(require 'packages-languages)

(when (daemonp) (server-mode))
